*** Settings ***
Resource	squash_resources.resource
Library		squash_tf.TFParamService

*** Keywords ***
Test Setup
	${__TEST_SETUP}	Get Variable Value	${TEST SETUP}
	${__TEST_1234_SETUP}	Get Variable Value	${TEST 1234 SETUP}
	Run Keyword If	$__TEST_SETUP is not None	${__TEST_SETUP}
	Run Keyword If	$__TEST_1234_SETUP is not None	${__TEST_1234_SETUP}

Test Teardown
	${__TEST_1234_TEARDOWN}	Get Variable Value	${TEST 1234 TEARDOWN}
	${__TEST_TEARDOWN}	Get Variable Value	${TEST TEARDOWN}
	Run Keyword If	$__TEST_1234_TEARDOWN is not None	${__TEST_1234_TEARDOWN}
	Run Keyword If	$__TEST_TEARDOWN is not None	${__TEST_TEARDOWN}

*** Test Cases ***
Un Test 1234 Test
	${NAME} =	Get Test Param	DS_NAME
	${AGE} =	Get Test Param	DS_AGE

	[Setup]	Test Setup

	Given A given keyword with no data
	Then A then keyword with name ${NAME} and age ${AGE}

	[Teardown]	Test Teardown