*** Settings ***
Resource	squash_resources.resource
Library		squash_tf.TFParamService

*** Variables ***
${NAME}    a name
${AGE}    189

*** Keywords ***
Test Setup
	${__TEST_SETUP}	Get Variable Value	${TEST SETUP}
	${__TEST_001_SETUP}	Get Variable Value	${TEST 001 SETUP}
	Run Keyword If	$__TEST_SETUP is not None	${__TEST_SETUP}
	Run Keyword If	$__TEST_001_SETUP is not None	${__TEST_001_SETUP}

Test Teardown
	${__TEST_001_TEARDOWN}	Get Variable Value	${TEST 001 TEARDOWN}
	${__TEST_TEARDOWN}	Get Variable Value	${TEST TEARDOWN}
	Run Keyword If	$__TEST_001_TEARDOWN is not None	${__TEST_001_TEARDOWN}
	Run Keyword If	$__TEST_TEARDOWN is not None	${__TEST_TEARDOWN}

*** Test Cases ***
Test01

	[Setup]	Test Setup

	Given A given keyword with no data
	Then A then keyword with name ${NAME} and age ${AGE}

	[Teardown]	Test Teardown