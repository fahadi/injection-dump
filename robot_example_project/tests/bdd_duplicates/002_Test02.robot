*** Settings ***
Resource	squash_resources.resource
Library		squash_tf.TFParamService

*** Variables ***
${NAME}    a name
${AGE}    189

*** Keywords ***
Test Setup
	${__TEST_SETUP}	Get Variable Value	${TEST SETUP}
	${__TEST_002_SETUP}	Get Variable Value	${TEST 002 SETUP}
	Run Keyword If	$__TEST_SETUP is not None	${__TEST_SETUP}
	Run Keyword If	$__TEST_002_SETUP is not None	${__TEST_002_SETUP}

Test Teardown
	${__TEST_002_TEARDOWN}	Get Variable Value	${TEST 002 TEARDOWN}
	${__TEST_TEARDOWN}	Get Variable Value	${TEST TEARDOWN}
	Run Keyword If	$__TEST_002_TEARDOWN is not None	${__TEST_002_TEARDOWN}
	Run Keyword If	$__TEST_TEARDOWN is not None	${__TEST_TEARDOWN}

*** Test Cases ***
Test02

	[Setup]	Test Setup

	Given A given keyword with no data
	Then A then keyword with name ${NAME} and age ${AGE}

	[Teardown]	Test Teardown